package lp.dlis;

public class DuplicateDataException extends Exception {
    public DuplicateDataException(String emailAlreadyExists) {

        super(emailAlreadyExists);
    }
}
