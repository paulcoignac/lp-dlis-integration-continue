package lp.dlis;

import javax.swing.plaf.synth.SynthTabbedPaneUI;
import java.util.ArrayList;
import java.util.List;

public class ContactDAO {
    private final List<String> emails;


    public ContactDAO() {
        emails = new ArrayList<>();
    }


    public void add(String email) throws DuplicateDataException {

        if (emails.contains(email)) {
            throw new DuplicateDataException("Email already exists");
        }


        emails.add(email);
    }

    public void delete(String email) throws NotFoundDataException {
        if (!emails.contains(email)) {
            throw new NotFoundDataException("Email does not exist");
        }
        emails.remove(email);

    }

    public List<String> fetchAll() {
        return emails;
    }
}
