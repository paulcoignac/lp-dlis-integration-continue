package lp.dlis;

public class NotFoundDataException extends Exception {
    public NotFoundDataException(String emailNotFound) {

        super(emailNotFound);
    }
}
