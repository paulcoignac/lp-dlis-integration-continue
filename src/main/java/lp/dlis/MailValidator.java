package lp.dlis;

public class MailValidator {
    private MailValidator() {
    }

    public static final String MAIL_REGEXP = "\\w{2,}@\\w{3,}\\.\\w{2,}";

    public static boolean isValid(String mail) {
        return mail != null && mail.matches(MAIL_REGEXP);
    }
}
