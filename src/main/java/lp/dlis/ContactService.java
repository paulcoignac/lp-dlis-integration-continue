package lp.dlis;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class ContactService {

    private ContactDAO dao = new ContactDAO();

    public void addContact(String email) throws DuplicateDataException {
        if (!MailValidator.isValid(email)) {
            throw new IllegalArgumentException("Invalid email");
        }
        dao.add(email);
    }

    public void deleteContact(String email) throws NotFoundDataException {
        dao.delete(email);
    }

    public List<String> listGetAllContact() throws ExecutionException, InterruptedException, TimeoutException {
        return Executors.newSingleThreadExecutor().submit(() -> dao.fetchAll()).get(3, TimeUnit.SECONDS);
    }
}
