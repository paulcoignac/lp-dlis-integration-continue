package lp.dlis;

import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.stubbing.Answer;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@MockitoSettings
class ContactServiceMockTest {

    @Mock
    private ContactDAO dao;

    @InjectMocks
    private ContactService contactService=new ContactService();


    @Test
    void duplicateEmail() throws DuplicateDataException {
        doThrow(new DuplicateDataException("email dupliqué"))
                .when(dao)
                .add("duplicate@duplicate.fr");
        assertThrows(DuplicateDataException.class, () -> contactService.addContact("duplicate@duplicate.fr"));
    }
    @Test
    void removeInvalidContact() throws NotFoundDataException {
        doThrow(new NotFoundDataException("email non trouvé"))
                .when(dao)
                .delete("unmail@gmail.com");
        assertThrows(NotFoundDataException.class, () -> contactService.deleteContact("unmail@gmail.com"));
    }
    @Test
    void removeValidContact() throws NotFoundDataException {
        doNothing().when(dao).delete("eleve1@univ.fr");
        contactService.deleteContact("eleve1@univ.fr");
    }
    @Test
    void listGetAllContactNotEmpty() throws ExecutionException, InterruptedException, TimeoutException {
        when(dao.fetchAll()).thenReturn(List.of(new String[]{"eleve1@univ.fr","eleve2@univ.fr","eleve3@univ.fr"}));
        assertEquals(contactService.listGetAllContact(), List.of(new String[]{"eleve1@univ.fr","eleve2@univ.fr","eleve3@univ.fr"}));
    }
    @Test
    void shouldInterruptFetch () {
        Mockito.when(dao.fetchAll()).thenAnswer((Answer<List<String>>) invocation -> {
            Thread.sleep(4000);
            return new ArrayList<>();
        });
        assertThrows(TimeoutException.class, () -> contactService.listGetAllContact());
    }
}
