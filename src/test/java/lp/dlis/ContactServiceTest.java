package lp.dlis;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class ContactServiceTest {
    private final ContactService contactService = new ContactService();

    @Test
    void addValidContact() throws DuplicateDataException {
        contactService.addContact("test@test.fr");
    }

    @Test
    void addInvalidContact() {
        assertThrows(IllegalArgumentException.class, () -> contactService.addContact("test"));
    }

    @Test
    void addNullContact() {
        assertThrows(IllegalArgumentException.class, () -> contactService.addContact(null));
    }

    @Test
    void addEmptyContact() {
        assertThrows(IllegalArgumentException.class, () -> contactService.addContact(""));
    }

    @Test
    void addBlankContact() {
        assertThrows(IllegalArgumentException.class, () -> contactService.addContact(" "));
    }

    @Test
    void addDuplicatedContact() throws DuplicateDataException {
        contactService.addContact("1contact@univ.fr");
        assertThrows(DuplicateDataException.class, () -> contactService.addContact("1contact@univ.fr"));
    }
}