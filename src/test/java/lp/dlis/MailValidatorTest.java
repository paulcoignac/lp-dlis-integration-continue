package lp.dlis;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class MailValidatorTest {
    @Test
    void isValid() {
        assertTrue(MailValidator.isValid("at@univ.fr"),"Cas normal 1");
        assertTrue(MailValidator.isValid("at@univ.com"),"Cas normal 2");
        assertTrue(MailValidator.isValid("at@uni.fr"),"Cas normal 3");
        assertFalse(MailValidator.isValid("@."), "Domaine et nom vide");
        assertFalse(MailValidator.isValid("@@@@@@......."), "trop de points et d'@");
        assertFalse(MailValidator.isValid("p@x.z"), "Domaine trop court");
        assertFalse(MailValidator.isValid("p@x"), "Pas d'extension de domaine et trop court");
        assertFalse(MailValidator.isValid("p@x."),"extension de domaine vide");
        assertFalse(MailValidator.isValid("p"), "Pas de @ et pas de nom de domaine");
        assertFalse(MailValidator.isValid("    @      .      "), "Caracteres blancs");
        assertFalse(MailValidator.isValid(""), "Chaine vide");
        assertFalse(MailValidator.isValid(null), "Pas de @ et pas de nom de domaine");
    }
}